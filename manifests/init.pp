class mpwar {

	##INSTALAMOS APACHE
	class { 'apache': 
	}

	##CREAMOS VIRTUAL HOSTS 
	apache::vhost { 'myMpwar.dev':
		port => '80',
		docroot => '/web/',
		docroot_owner => 'vagrant',
		docroot_group => 'vagrant',
	}

	##INSTALAMOS MYSQL
	include '::mysql::server'

	##CREAMOS LA BASE DE DATOS mympwar
	mysql::db { 'mympwar':
	  user     => 'myuser',
	  password => 'mypass',
	  host     => 'localhost',
	}

	##INSTALAMOS PHP 5.5
	$yum_repo = 'remi-php55'
	include ::yum::repo::remi_php55

	class { 'php':
	  version => 'latest',
	  require => Yumrepo[$yum_repo]
	}

	#INSTALAMOS MEMCACHED
	include memcached
	
	#Incluimos el modulo que nos genera index.php y info.php
	include indexmod
	
	#CREAMOS  HOSTS MEMCACHED
	host{ 'localhost':
		ensure => 'present',
		target => '/etc/hosts',
		ip => '127.0.0.1',
		host_aliases => ['memcached']
	}
	
	#Incluimos el modulo IPTABLES en el que hemos configurado que 
	#permita puertos de entrada '80' y '8080'
	include iptables

}
